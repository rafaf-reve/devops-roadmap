# Action List

## ToDo 

**Application Refactoring**

- [ ] Preparing Dockerfile for the service
- [ ] Preparing Docker image of the service and save it to docker registry
- [x] Preparing `systemctl` unit file for the service
  - [ ] Updating heap size variable to properly tune service for production use
- [ ] Updating proper documentation
  - [ ] Updating readme file in the repository listing all dependency
- [ ] running service as nonroot user


**OS Config**

- [ ] Integrating service to blackbox monitoring and alerting
- [ ] Persisting OS Level necessary configs i.e. ulimit, **firewall**, nginx confs and certs
- [ ] Fixing proper hostname of the OS

**Ansible**

- [ ] Preparing Ansible script that 
  - [ ] deploy service 
  - [ ] take care of necessary application configuration 
  - [ ] Automatically add service under monitoring 
  - [ ] Persist OS level nessary configuration i.e ulimit, firewall, nginx and others.
  - [ ] Test ansible script in UAT. 
    - [ ] Testing ansible script from scratch 
    - [ ] Testing ansible script in failover scenario.
  - [ ] Deploy Service with ansible in Prod


## Dependency: 

- Need bitbucket access of the repository
- Preparing docker registry for internal use
- Need OS access

**CI/CD**
- [ ] Preparing jenkins job for smooth integration in staging and production.

# File Upload Server **Refactoring** [Stateful -> Stateless]

It's very very important. File Server is getting bigger and bigger. Currently it's ~ 2TB. Soon it will be out of our hand to properly operate it. 

Phase 1: Intermediate Testing Phase

- [ ] Migrating file storage to Linode Object storage and mounting it in the OS.
- [ ] Serving files Object storage 

Phase 2: Final Phase

- [ ] Refactoring Service to work with linode object storage api and retrieve and download file.