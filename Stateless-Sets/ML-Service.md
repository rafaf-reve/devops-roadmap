# Action List

## ToDo 

**Application Refactoring**

- [ ] Preparing Dockerfile for the service
- [ ] Preparing Docker image of the service and save it to docker registry
- [x] Preparing `systemctl` unit file for the service
  - [ ] Tuning service with gunicorn to scale it for production.
- [ ] Updating proper documentation
  - [ ] Updating readme file in the repository listing all dependency and installation procedure.
- [ ] running service as nonroot user


**OS Config**

- [ ] Integrating service to blackbox monitoring and alerting
- [ ] Persisting OS Level necessary configs i.e. ulimit, **firewall**, nginx confs and certs
- [ ] Fixing proper hostname of the OS

**Ansible**

- [ ] Preparing Ansible script that 
  - [ ] deploy service 
  - [ ] take care of necessary application configuration 
  - [ ] Automatically add service under monitoring 
  - [ ] Persist OS level nessary configuration i.e ulimit, firewall, nginx and others.
  - [ ] Test ansible script in UAT. 
    - [ ] Testing ansible script from scratch 
    - [ ] Testing ansible script in failover scenario.
  - [ ] Deploy Service with ansible in Prod

**CI/CD**
- [] Preparing jenkins job for smooth integration in staging and production.

## Dependency: 

- Need bitbucket access of the repository
- Preparing docker registry for internal use
- Need OS access