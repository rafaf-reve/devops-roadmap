# Action List

## ToDo 

**Application Refactoring**

- [ ] Preparing Dockerfile for the service
- [ ] Preparing Docker image of the service and save it to docker registry
- [ ] Preparing `systemctl` unit file for the service
- [ ] Updating readme file in the repository listing all dependency and installation procedure.


**OS Config**

- [ ] Integrating service to blackbox monitoring and alerting
- [ ] Persisting OS Level necessary configs i.e. ulimit, firewall, nginx confs and certs

**Ansible**

- [ ] Preparing Ansible script that 
  - [ ] deploy service 
  - [ ] take care of necessary application configuration 
  - [ ] Automatically add service under monitoring 
  - [ ] Persist OS level nessary configuration i.e ulimit, firewall, nginx and others.
  - [ ] Test ansible script in UAT. 
    - [ ] Testing ansible script from scratch 
    - [ ] Testing ansible script in failover scenario.
  - [ ] Deploy Service with ansible in Prod


## Dependency: 

- Need bitbucket access of the repository
- Preparing docker registry for internal use
- Need OS access

# List of stateless services

1. Dashboard Service
2. Dashboard Backend
3. File Upload Service (Currenly it's stafeful, but it needs to be stateless)
4. ML Service 
5. STUN/TURN Server 
6. chat-server 
7. Social Gateway
8. Chat Aggregation Service
9. Location Service

# Next

1. Preparing `docker-compose.yml` to up and run all our services with a single command
2. Preparing compatible k8s helm chart to run services.
3. Need to migrate from block storge to object storage for File Upload Service.
4. Preparing application package (apt/yum) and deploy it to repository.