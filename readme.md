# A roadmap to achieve best practices for production Infrustructure 
```
Monolithic -------> MicroService 
Manual     -------> Automated
```

## Goal

1. Kubernetes Implementation of ReveChat App
2. 0 Downtime production deployment
3. Package and Artifactory Management
4. Establishing proper monitoring and Alerting System and making failover checklist
5. MySQL Clustering and BackUp
6. Redis Server / Sentinel High Availability Management
7. MongoDB Cluster H/A 
8. Configuration Management 
9.  Proper Technical Documentation

## RoadMap

**Stateful Set:**

1. MySQL Database
2. Redis Database
3. MongoDB Database 

***ToDo***

1. Need to install proper monitoring and alerting system.
   1. Need to establish failover checklist for every alert.
      1. Level 1 Emergency : CPU 100% Memory 80% Disk 80%
      2. Level 2 Emergency : CPU 150% Memory 90% Disk 90%
      3. Level 3 Emergency : Service failure

2. Need to create a proper back up and restore system so that each service can be totally recovered from failure within 3 minutes.
   1. Need to prepare ansible script to install service.
   2. Need to back up configuration of each servcie/instance. Any changes in configuration must be back up and documented.

3. Need to ensure high availability with proper scaling.




**Stateless Set:**

1. Dashboard Service
2. Dashboard Backend
3. File Upload Service (Currenly it's stafeful, but it needs to be stateless)
4. ML Service 
5. STUN/TURN Server 
6. chat-server 
7. Social Gateway
8. Chat Aggregation Service
9. Location Service



***ToDo***

1. Establishing a proper monitoring system to monitor existing infrustructure and plan how to response alerts.
    - Monitoring System : Prometheus
    - Dashboard : Grafana 
    - Task Status : Ongoing
    - Ensuring all production and uat instances are monitored with node_exporter
    - MySQL, mongoDB and Redis services are monitored
    - Service Discovery is used to check api endpoint 
    - production service endpoint health check with blackbox exporter

2. Configuring process manager `systemctl` for each service
    - Task Status: On going 

3. Migrating from Volume Storage to Object Storage 
    - Phase 1: Copying all volume data to Object Storage
    - Phase 2.1: mounting Object storage to instance 
    - Phase 2.2: Updating codebase to work with object storage.

4. Installing Jenkins and Enabling proper CI/CD channel for services. Need to handle all software development lifecycle with care so that each phase is automated.

5. Installing Artifact repository to store artifact with proper versioning. We need to ensure update of the software is stored in proper repository so that production update can be smooth.
    - jFrong / Nexus [Need to discuss with devs]
    - docker registry
    
6. Containerizing Existing Service. Packaging image and deploy image to repository.
    - Breaking services in to multiple independant part
    - Ensuring low fire up timing 
    
7. Installing kubernetis and migrating services one by one to kubernetes cluster.




Meeting Points: 19 Sept 2021

1. We need to make sure H/A for MySQL and Redis Database
2. We need to make failover recovery for MySQL and Redis Database with all instance configurations. Ensuring all ulimit, db_connection etc conf.
3. Nur Nabi Bhai will check cloud options and their rate. Current Cloud Options - AWS , Digital Ocean
4. Rafaf will prapare a roadmap (this one) and further discussion will be on it.


