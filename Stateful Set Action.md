# Action List

## ToDo

- [ ] Need to configure periodical back up.
- [ ] Need to prapare single script to restore back up.
- [ ] Attach system to monitoring and alert.
- [ ] Preparing ToDo checklist for each level of alert.
- [ ] Preparing HA Setup for each service
- [ ] Persisting OS level and service level configurations.
  - [ ] Documenting it
  - [ ] Preparing an ansible script to deploy the service with all service level and application level configuration.
    - [ ] From Scratch
    - [ ] In a failover scenario
  - [ ] Testing ansible script in UAT and production.
  


# List of stateful sets

1. MySQL
2. MongoDB
3. Redis


